MODULES      := stout_smearing.tex anisotropy.tex propagator.tex \
	              mu-squared.tex hadspec.tex

TARGETS      := $(MODULES:%.tex=%.pdf)
AUX_SYMLINKS := $(MODULES:%.tex=%.aux)
DEFS         := $(wildcard definitions/*.tex)

.PHONY: all
all: $(TARGETS) $(AUX_SYMLINKS)

BUILD_DIRECTORY := build
PWD := $(shell pwd)

%.pdf: %.tex $(DEFS) | $(BUILD_DIRECTORY)
	@cd $(BUILD_DIRECTORY) &&\
	ln -fs ../definitions . &&\
	ln -fs ../references.bib . &&\
	if latexmk -pdf -xelatex -g ../$<; then \
		mv $@ $(PWD); \
	else \
		latexmk -c ../$<; fi

%.aux: %.pdf
	@ln -fs $(BUILD_DIRECTORY)/$@ .

$(BUILD_DIRECTORY):
	@mkdir -p $@

.PHONY: clean
clean:
	@rm -rf $(TARGETS)
	@rm -rf $(AUX_SYMLINKS)
	@rm -rf $(BUILD_DIRECTORY)
