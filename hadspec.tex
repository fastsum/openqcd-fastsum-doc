\documentclass[]{article}
\input{definitions/packages.tex}
\input{definitions/style.tex}
\input{definitions/macros.tex}

\addbibresource{references.bib}


% ------------- %
% Main document %
% ------------- %

\begin{document}

\noindent
\begin{tikzpicture}
  \node[scale=2] (title) {Hadron spectra with \texttt{openqcd-hadspec}}; 
  \draw[thick] (title.south west) -- +(\textwidth,0);
  \node[anchor=north west,font=\strut{}]
    at ([yshift=-1mm] title.south west) {Jonas Rylund Glesaaen};
  \node[anchor=north east,font=\strut{}]
    at ([shift={(\textwidth,-1mm)}] title.south west) {September 2018};
\end{tikzpicture}

\vspace{.5cm}

\section{Parameters for the hadron spectrum calculation}

\lstset{%
  language={},
  basicstyle=\ttfamily\fontsize{8pt}{8pt}\selectfont,
  identifierstyle={}}

The infiles are for the most part identical to those in the vanilla openQCD
simulations as well as those of openqcd-propagator. See the input file examples
in \lstinline!main/examples! to see how they are specified. The section that is
unique to openqcd-hadspec is
%
\begin{lstlisting}
    [Hadspec]
    quarks 0 0 1 2
    names u d s c

    [Quark 0]
    im0    0
    isolv  0
    smear  1

    [Quark 1]
    im0    1
    isolv  0
    smear  1

    [Quark 2]
    im0    2
    isolv  0
    smear  1
    ani    1.078 4.3 1.3545694350 0.7939900651 
\end{lstlisting}
%
The first \lstinline!Hadspec! section tells the program which quarks to build
the hadron spectrum from, the actual parameters for the individual quarks are
specified in quark \lstinline!Quark! sections. The \lstinline!Hadspec! section
has two parameters
%
\begin{description}[align=left]
  \item [quarks (int{[]})] \hfill\\
    Indices of the participating quarks. These can be repeating which indicates
    degenerate quarks. Repeated indices are treated as different flavours when
    it comes to constructing hadronic operators.
  \item [names (char{[]})] \hfill ({\itshape optional}, {\lstfont =quarks})\\
    The accompanying single-character names for the quarks. Exists to make the
    output of the program easier identify. E.g. the nucleon in this example
    would be named "\lstinline!doublet.uud!" instead of the less useful
    "\lstinline!doublet.001!" which it would be if names were not provided.
\end{description}
%
The \lstinline!Quark! section parameters have the following interpretations
%
\begin{description}[align=left]
  \item [im0 (int)] \hfill\\
    Index of the bare mass of this quark. Bare masses specified by
    \lstinline!kappa! in the \lstinline!Dirac operator! section.
  \item [isolv (int)] \hfill\\
    Index of the solver to use when computing the quark propagator.
  \item [smear (boolean)] \hfill\\
    Whether or not so smear the gauge field for its Dirac operator.
  \item [ani (double{[4]})] \hfill ({\itshape optional}, {\lstfont =ani\_params()})\\
    The anisotropy parameters to use for its Dirac operator. If this is omitted
    the parameters specified in \lstinline!Anisotropy parameters! will be used.
\end{description}

\section{Hadronic correlation functions}

We define all hadronic correlation functions to be of the form
%
\begin{equation}
  \mathcal{G} = \tr \big[ P \big\langle \mathcal{O} \, \longbar{\mathcal{O}} \big\rangle \big]
\end{equation}
%
where $P$ is some sort of projection operator, $\mathcal{O}$ is the hadronic
operator of choice, $\langle \cdot \rangle$ is a configuration average as well
as an integration over fermionic fields, and $\tr[]$ is a spin-colour trace.
If we want to stress that the fermionic degrees of freedom have been integrated
out we write $\langle \cdot \rangle_B$, whereas if we want to symbolise the
fermionic integral we write $\langle \cdot \rangle_F$.

\subsection{Mesonic correlation function}

In the case of the mesonic correlation functions the expression is as one would
expect
%
\begin{equation}
  \mathcal{G}_M(t) = %
    \sum_{\vec{x}} \big\langle \tr \big[
        \gamma_5 \Gamma^{\dagger} Q_1(x, 0) \Gamma \gamma_5 Q_2^{\dagger}(x,0)
      \big] \big\rangle_B.
\end{equation}

\subsection{Hadronic correlation function}

For the hadronic correlation function we first have to choose our operators
$\mathcal{O}$. We choose the same operators as defined in
\cite{Leinweber:2004it}, namely
%
\begin{alignat}{99}
  \chi^{2 \mathrm{fl}}_{\gamma} &= &&\epsilon_{abc} q_{1,\gamma}^{a} \big( 
  q_{1,\alpha}^{b} \big[ C \gamma_5 \big]_{\alpha\beta} q_{2,\beta}^{c} \big),
    && (J=1/2) \vphantom{\Big]}\\
  \chi^{1 \mathrm{fl}}_{\gamma,\mu} &= &&\epsilon_{abc} q_{1,\gamma}^{a} \big( 
  q_{1,\alpha}^{b} \big[ C \gamma_{\mu} \big]_{\alpha\beta} q_{1,\beta}^{c} \big),
    && (J=3/2) \vphantom{\Big]}\\
  \chi^{2 \mathrm{fl}}_{\gamma,\mu} &= \frac{1}{\sqrt{3}} &&\epsilon_{abc} \Big[
    2 q_{1,\gamma}^{a} \big( q_{1,\alpha}^{b} \big[ C \gamma_{\mu} \big]_{\alpha\beta} q_{2,\beta}^{c} \big)
    + q_{2,\gamma}^{a} \big( q_{1,\alpha}^{b} \big[ C \gamma_{\mu} \big]_{\alpha\beta} q_{1,\beta}^{c} \big)
  \Big],
  \hspace{1cm} && (J=3/2)
\end{alignat}
%
where $C = i \gamma_0 \gamma_2$ is the charge conjugation matrix. We see that
all states have an open spinor index, $\gamma$, due to the fact that they are
fermions; the $J=3/2$ states also have an open Lorentz index. We therefore have
to project the states onto the physical states we are interested in for the
analysis. One should also not that although the two last states are labelled as
spin-states $J = 3/2$, they are actually a superposition of both $J=1/2$ and
$J=3/2$ states.  We therefore also have to project out the $J=3/2$ component of
these operators.  Once more we follow the procedure outlined in
\cite{Leinweber:2004it} for this.

The first step of the calculation is to carry out the Grassmann integrals, i.e.
the Wick contractions. These turn out to be
%
\begin{align}
  \big\langle \chi_{\gamma} \bar{\chi}_{\gamma'} \big\rangle_F^{2 \mathrm{fl}}
  = \hphantom{\pm} &\epsilon^{abc} \epsilon^{a'b'c'}
    \Big\{ Q_1^{aa'} \big[
        \hphantom{2} \tr(C \gamma_5 Q_2^{T, bb'} C \gamma_5 Q_1^{cc'})
        + \hphantom{4} C \gamma_5 Q_2^{T, bb'} C \gamma_5 Q_1^{cc'}
    \big] \Big\}_{\gamma\gamma'}, \\
  \big\langle \chi_{\mu,\gamma} \bar{\chi}_{\nu,\gamma'} \big\rangle_F^{1 \mathrm{fl}}
  = \pm &\epsilon^{abc} \epsilon^{a'b'c'}
    \Big\{ Q_1^{aa'} \big[
        2 \tr(C \gamma_{\mu} Q_1^{T, bb'} C \gamma_{\nu} Q_1^{cc'})
        + 4 C \gamma_{\mu} Q_1^{T, bb'} C \gamma_{\nu} Q_1^{cc'}
    \big] \Big\}_{\gamma\gamma'}, \\
  \big\langle \chi_{\mu,\gamma} \bar{\chi}_{\nu,\gamma'} \big\rangle_F^{2 \mathrm{fl}}
  = \pm &\epsilon^{abc} \epsilon^{a'b'c'}
    \Big\{ Q_1^{aa'} \big[
        2 \tr(C \gamma_{\mu} Q_2^{T, bb'} C \gamma_{\nu} Q_1^{cc'})
        + 4 C \gamma_{\mu} Q_2^{T, bb'} C \gamma_{\nu} Q_1^{cc'}
    \big] \Big\}_{\gamma\gamma'} \nonumber \\
  \pm &\epsilon^{abc} \epsilon^{a'b'c'}
    \Big\{ Q_1^{aa'} \big[
        2 \tr(C \gamma_{\mu} Q_1^{T, bb'} C \gamma_{\nu} Q_2^{cc'})
        + 4 C \gamma_{\mu} Q_1^{T, bb'} C \gamma_{\nu} Q_2^{cc'}
    \big] \Big\}_{\gamma\gamma'} \nonumber \\
  \pm &\epsilon^{abc} \epsilon^{a'b'c'}
    \Big\{ Q_2^{aa'} \big[
        2 \tr(C \gamma_{\mu} Q_1^{T, bb'} C \gamma_{\nu} Q_1^{cc'})
        + 4 C \gamma_{\mu} Q_1^{T, bb'} C \gamma_{\nu} Q_1^{cc'}
    \big] \Big\}_{\gamma\gamma'}.
\end{align}
%
The trace matrix multiplications are all in spinor indices; the same applies to
the trace and the transpose. In these expressions we have used the fact that
$(C\gamma_5)^T = C\gamma_5$, $\overline{C \gamma_5} = C\gamma_5$,
$(C\gamma_{\mu})^T = C\gamma_{\mu}$, and $\overline{C \gamma_{\mu}} = \pm C\gamma_{\mu}$.
$Q_1$ and $Q_2$ are the propagators for quark flavour 1 and 2 respectively.
Since these all have a similar structure we can for brevity write them in a
somewhat compact form
%
\begin{align}
  \big\langle \chi \bar{\chi} \big\rangle_F^{2 \mathrm{fl}}
  = \hphantom{\pm} &\epsilon\epsilon Q_1 \big[
  \hphantom{2}\tr(\Lambda_5 Q_2^T \Lambda_5 Q_1) + \hphantom{4}\Lambda_5 Q_2^T \Lambda_5 Q_1 \big], \\
  \big\langle \chi_{\mu} \bar{\chi}_{\nu} \big\rangle_F^{1 \mathrm{fl}}
  = \pm &\epsilon\epsilon Q_1 \big[
  2 \tr(\Lambda_{\mu} Q_1^T \Lambda_{\nu} Q_1) + 4 \Lambda_{\mu} Q_1^T \Lambda_{\nu} Q_1 \big], \\
  \big\langle \chi_{\mu} \bar{\chi}_{\nu} \big\rangle_F^{2 \mathrm{fl}}
  = \pm &\epsilon\epsilon Q_1 \big[
  2 \tr(\Lambda_{\mu} Q_2^T \Lambda_{\nu} Q_1) + 4 \Lambda_{\mu} Q_2^T \Lambda_{\nu} Q_1 \big] \nonumber \\
  \pm &\epsilon\epsilon Q_1 \big[
  2 \tr(\Lambda_{\mu} Q_1^T \Lambda_{\nu} Q_2) + 4 \Lambda_{\mu} Q_1^T \Lambda_{\nu} Q_2 \big] \nonumber \\
  \pm &\epsilon\epsilon Q_2 \big[
  2 \tr(\Lambda_{\mu} Q_1^T \Lambda_{\nu} Q_1) + 4 \Lambda_{\mu} Q_1^T \Lambda_{\nu} Q_1 \big],
\end{align}
%
where $\Lambda_{\mu} = C \gamma_{\mu}$ and $\Lambda_5 = C \gamma_5$.

The next step is to carry out the necessary projections. For the parity
projection operator we use
%
\begin{equation}
  P^{\pm} = \frac{1 \pm \gamma_0}{2}.
\end{equation}
%
The spin $J=3/2$ projection operator is naturally a rank-2 tensor, and is of the
form
%
\begin{equation}
  P^{3/2}_{\mu\nu} = g_{\mu\nu}
    - \scalebox{0.8}{$\displaystyle\frac{1}{3}$}\gamma_{\mu}\gamma_{\nu}.
\end{equation}
%
We can therefore construct a set of valid spin 3/2 states
%
\begin{equation}
  G^{3/2}_{\alpha\beta} = P^{3/2}_{\alpha,\mu} \big\langle \chi_{\mu} \bar{\chi}_{\beta} \big\rangle.
\end{equation}
%
Now that we have all the projections we need, we define our final states
%
\begin{align}
  N &= \tr \Big[ P^{+} \big\langle \chi \bar{\chi} \big\rangle^{2 \mathrm{fl}} \Big], \\
  \Delta^{+}_{\alpha\beta} &= \tr \Big[ P^{+} P^{3/2}_{\alpha\mu}
    \big\langle \chi_{\mu} \bar{\chi}_{\beta} \big\rangle^{2 \mathrm{fl}} \Big], \\
  \Delta^{++}_{\alpha\beta} &= \tr \Big[ P^{+} P^{3/2}_{\alpha\mu}
    \big\langle \chi_{\mu} \bar{\chi}_{\beta} \big\rangle^{1 \mathrm{fl}} \Big].
\end{align}
%
Again, following \cite{Leinweber:2004it}, we choose the $J=3/2$ particles to be
defined by ${\alpha,\beta} = 3$, i.e. $\Delta^+ = \Delta^+_{33}$ and
$\Delta^{++} = \Delta^{++}_{33}$. The particles here are were named as the
particle they represent in the case that $q_1 = u$ and $q_2 = d$, by using
different constituents other hadrons can be computed.

\subsection{Source smearing and sink smearing}

Source smearing has already been covered in the openqcd-propagator
documentation, and we will only do the most basic definition. A source smeared
propagator is one where the smearing operator is applied to the field before the
propagation operator is, namely
%
\begin{equation}
  Q^{PS} = D^{-1} S \psi.
\end{equation}
%
In this expression $\psi$ is the bare source and $S$ is the smearing operator.
Conversely, a sink smeared operator is then defined by applying the smearing
operator after the propagation operator
%
\begin{equation}
  Q^{SP} = S D^{-1} \psi.
\end{equation}
%
One can naturally also define for example a smeared-smeared propagator
%
\begin{equation}
  Q^{SS} = S D^{-1} S \psi.
\end{equation}
%
which is what is currently used if smearing is specified in the code.

\section{$\gamma$-matrix definitions} \label{sec:gamma}

We use the $\gamma$-matrices as defined by Lüscher \cite{dirac-operator}. Thus
we have
%
\begin{equation}
  \gamma_{\mu} = \begin{pmatrix} 0 & e_{\mu} \\ (e_{\mu})^{\dagger} & 0 \end{pmatrix}
  \hspace{.5cm} \text{where} \hspace{.5cm} e_0 = -1,\: e_i = - i \sigma_i.
\end{equation}
%
Furthermore, $\gamma_5 = \gamma_0 \gamma_1 \gamma_2 \gamma_3$, and
$\sigma_{\mu\nu} = \frac{i}{2} \big[ \gamma_{\mu}, \gamma_{\nu} \big]$. With
these definitions we define our 16 $\Gamma$-matrices to be
%
\begin{alignat*}{99}
  \Gamma_0 &= \mathbf{1}, & \hspace{1cm}
    \Gamma_4 &= \gamma_3, & \hspace{1cm}
    \Gamma_8 &= \gamma_5 \gamma_2, & \hspace{1cm}
    \Gamma_{12} &=  \sigma_{0, 3}, \\
  \Gamma_1 &= \gamma_0, &  \hspace{1cm}
    \Gamma_5 &= \gamma_5, & \hspace{1cm}
    \Gamma_9 &= \gamma_5 \gamma_3, & \hspace{1cm}
    \Gamma_{13} &= \sigma_{1,2}, \\
  \Gamma_2 &= \gamma_1, & \hspace{1cm}
    \Gamma_6 &= \gamma_5 \gamma_0, & \hspace{1cm}
    \Gamma_{10} &= \sigma_{0, 1}, & \hspace{1cm}
    \Gamma_{14} &= \sigma_{1,3}, \\
  \Gamma_3 &= \gamma_2, & \hspace{1cm}
    \Gamma_7 &= \gamma_5 \gamma_1, & \hspace{1cm}
    \Gamma_{11} &= \sigma_{0, 2}, & \hspace{1cm}
    \Gamma_{15} &= \sigma_{2,3}.
\end{alignat*}

\printbibliography

\end{document}
