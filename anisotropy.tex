\documentclass[]{article}
\input{definitions/packages.tex}
\input{definitions/style.tex}
\input{definitions/macros.tex}

\addbibresource{references.bib}


% ------------- %
% Main document %
% ------------- %

\begin{document}

\noindent
\begin{tikzpicture}
  \node[scale=2] (title) {Anisotropic actions for openQCD}; 
  \draw[thick] (title.south west) -- +(\textwidth,0);
  \node[anchor=north west,font=\strut{}]
    at ([yshift=-1mm] title.south west) {Jonas Rylund Glesaaen, Benjamin J\"{a}ger};
  \node[anchor=north east,font=\strut{}]
    at ([shift={(\textwidth,-1mm)}] title.south west) {March 2018};
\end{tikzpicture}

\vspace{.5cm}

\section{Parameters for the anisotropic action}

\lstset{%
  language={},
  basicstyle=\ttfamily\fontsize{8pt}{8pt}\selectfont,
  identifierstyle={}}

The anisotropy is activated by adding the following section to your
program configuration files for the \lstinline{qcd1, ym1, ms1, ms2} \&
\lstinline{ms4} executables.
%
\begin{lstlisting}
    [Anisotropy parameters]
    use_tts    0
    nu         1.5
    xi         4.3
    cR         1.5
    cT         0.9
    us_gauge 	 0.7
    ut_gauge	 1.0
    us_fermion 1.0
    ut_fermion 1.0
\end{lstlisting}
%
The block has been made optional to be compatible with openQCD 1.6 configuration
files. If no such section is specified, the resulting action will be the
isotropic action as specified by the other options. The parameters have the
following interpretations:

\begin{description}[align=left]
  \item [use\_tts(boolean)] \hfill\\
    Determines whether the gauge action should include rectangles that extend
    twice into the temporal direction. See section \ref{sec:gauge_action}
  \item [nu (double)] \hfill\\
    Sets the $\nu$ parameter as defined in section \ref{sec:fermion_action}
  \item [xi (double)] \hfill\\
    Sets the $\xi_0$ parameter as defined in section \ref{sec:gauge_action}
  \item [cR (double)] \hfill\\
    Sets the anisotropic clover parameter $c_R$ as defined in eq.~\eqref{eq:aniso_dirac_op}
  \item [cT (double)] \hfill\\
    Sets the anisotropic clover parameter $c_T$ as defined in eq.~\eqref{eq:aniso_dirac_op}
  \item [us\_gauge (double)] \hfill ({\itshape optional}, {\lstfont =1.0})\\
    Sets the tadpole improvement factor for spatial links in the gauge action
    ($u_s$)\\as defined in eqs.~(\ref{eq:aniso_gauge_use_tts},\ref{eq:aniso_gauge_no_tts})
  \item [ut\_gauge (double)] \hfill ({\itshape optional}, {\lstfont =1.0})\\
    Sets the tadpole improvement factor for temporal links in the gauge action
    ($u_s$)\\as defined in eqs.~(\ref{eq:aniso_gauge_use_tts},\ref{eq:aniso_gauge_no_tts})
  \item [us\_fermion (double)] \hfill ({\itshape optional}, {\lstfont =1.0})\\
    Sets the tadpole improvement factor for spatial links in the fermion action
    ($\tilde{u}_s$)\\as defined in eq.~\eqref{eq:aniso_dirac_op}
  \item [ut\_fermion (double)] \hfill ({\itshape optional}, {\lstfont =1.0})\\
    Sets the tadpole improvement factor for temporal links in the fermion action
    ($\tilde{u}_t$)\\as defined in eq.~\eqref{eq:aniso_dirac_op}
\end{description}

\section{The anisotropic action}

We use the anisotropic action as defined in \cite{Edwards:2008ja} with minor
alterations to allow for user defined $c_0$ and $c_1$ factors. The gauge- and
fermion actions are summarised in the next two subsections.

\subsection{Gauge action} \label{sec:gauge_action}

The anisotropic gauge action can take two forms depending on the {\lstfont
use\_tts} setting. By setting this flag one determines whether the rectangular
plaquette part of the improvement scheme for the gauge action is allowed to
extend in all directions, or in the spatial directions only.

\subsubsection{{\ttfamily use\_tts = 1}}

In this case the gauge action is isotropic besides the different weights applied
to the different directions and thus takes the same form in all directions
%
\begin{multline}\label{eq:aniso_gauge_use_tts}
  S_G = \frac{\beta}{N_c \gamma_g} \bigg\{
    \sum_{x,s > s'} \bigg[
      \frac{c_0}{u_s^4} P_{ss'}(x) + \frac{c_1}{u_s^6} \big(R_{ss'}(x) + R_{s's}(x)\big)
    \bigg] \\
    +\gamma_g^2 \sum_{x,s} \bigg[
      \frac{c_0}{u_s^2 u_t^2} P_{st} + \frac{c_1}{u_s^4 u_t^2} R_{st}(x)
      - \frac{c_1}{u_s^2 u_t^4} R_{ts}(x)
    \bigg]
  \bigg\}.
\end{multline}
%
In this equation $u_s$ and $u_t$ are the tadpole improvement factors for the
spatial- and temporal links in the gauge action respectively. $\gamma_g =
1/\xi_0$ where $\xi_0$ is the bare gauge anisotropy, and the plaquettes are
defined as
%
\begin{equation}
  P_{\mu\nu} = 
  \begin{tikzpicture}[baseline={([yshift=-.5ex] 0,.5)}]
    \draw[link] (0,0) -- (1,0) -- (1,1) -- (0,1) -- cycle;
    \node[anchor=north] at (0.5,0) [scale=0.8] {$\hat{\mu}$};
    \node[anchor=west] at (1,0.5) [scale=0.8] {$\hat{\nu}$};
  \end{tikzpicture}, \hspace{1cm}
  R_{\mu\nu} = 
  \begin{tikzpicture}[baseline={([yshift=-.5ex] 0,.5)}]
    \draw[link] (0,0) -- (1,0) -- (2,0) -- (2,1) -- (1,1) -- (0,1) -- cycle;
    \draw[dashed] (1,0) -- (1,1);
    \node[anchor=north] at (1,0) [scale=0.8] {$\hat{\mu}$};
    \node[anchor=west] at (2,0.5) [scale=0.8] {$\hat{\nu}$};
  \end{tikzpicture}.
\end{equation}
%
Specifically this action includes both $R_{st}$ and $R_{ts}$.

\subsubsection{{\ttfamily use\_tts = 0}}

For this second case we exclude rectangular loops that extend twice into the
temporal direction. This is the most common choice for improved anisotropic
gauge actions. The action then takes the form
%
\begin{equation}\label{eq:aniso_gauge_no_tts}
  S_G = \frac{\beta}{N_c \gamma_g} \bigg\{
    \sum_{x,s > s'} \bigg[
      \frac{c_0}{u_s^4} P_{ss'}(x) + \frac{c_1}{u_s^6} \big(R_{ss'}(x) + R_{s's}(x)\big)
    \bigg]
    +\gamma_g^2 \sum_{x,s} \bigg[
      \frac{c_0 + 4c_1}{c_0}\frac{c_0}{u_s^2 u_t^2} P_{st}
      + \frac{c_1}{u_s^4 u_t^2} R_{st}(x)
    \bigg]
  \bigg\},
\end{equation}
%
which one can see lacks the $R_{ts}$ term. The additional prefactor in front of
$P_{st}$ is chosen in such a way that the series expansion in $a$ for the
spatial and temporal components match up. Specifically if we write down this
series expansion\cite{Lepage:1997bh}
%
\begin{align}
  P_{\mu\nu} &= 1 - \frac{1}{6} a^4 \tr \big(g F_{\mu\nu} \big)^2
    - \frac{1}{72} a^6 \tr \Big( g F_{\mu\nu} \big(D_{\mu}^2 + D_{\nu}^2) g F_{\mu\nu}\Big)
    + \mathcal{O}\big(a^8\big), \\
  R_{\mu\nu} &= 1 - \frac{4}{6} a^4 \tr \big(g F_{\mu\nu} \big)^2
    - \frac{4}{72} a^6 \tr \Big( g F_{\mu\nu} \big(4 D_{\mu}^2 + D_{\nu}^2) g F_{\mu\nu}\Big)
    + \mathcal{O}\big(a^8\big),
\end{align}
%
we see that the spatial contributions are (ignoring tadpole improvement)
%
\begin{equation}
  S_{G,ss'} \sim \frac{1}{6} (c_0 + 8 c_1) a^4 \tr \big(g F_{ss'} \big)^2
    + \frac{1}{72} (c_0 + 20 c_1) a^6 \tr \Big( g F_{ss'} \big(D_{s}^2 + D_{s'}^2) g F_{ss'}\Big).
\end{equation}
%
The temporal contribution on the other hand is
%
\begin{equation}
  S_{G,st} \sim \frac{1}{6} (c_0 + 4 c_1) a^4 \tr \big(g F_{st} \big)^2
    + \frac{1}{72} (c_0 + 16 c_1) a^6 \tr \Big( g F_{ss'} D_{s}^2 g F_{ss'}\Big)
    + \frac{1}{72} (c_0 + 4 c_1) a^6 \tr \Big( g F_{ss'} D_{t}^2 g F_{ss'}\Big).
\end{equation}
%
If we consider a gauge action in which we scale the temporal contributions
independently
%
\begin{equation}
  S_{G} \sim c_0 P_{ss'} + c_1 \big(R_{ss'} + R_{s's}\big) + x c_0 P_{st} + y c_1 R_{st},
\end{equation}
%
then we can match the first two terms in the series expansions in the lattice
spacing $a$ by setting
%
\begin{equation}
  x = \frac{c_0 + 4c_1}{c_0}, \hspace{.5cm} y = 1.
\end{equation}
%
For a Symanzik improved action
%
\begin{equation}
  c_0 = \frac{5}{3}, \hspace{5mm} c_1 = -\frac{1}{12},
\end{equation}
%
we recover the action in \cite{Edwards:2008ja}
%
\begin{equation}
  S_G = \frac{\beta}{N_c \gamma_g} \bigg\{
    \sum_{x,s > s'} \bigg[
      \frac{5}{3u_s^4} P_{ss'}(x) - \frac{1}{12 u_s^6} \big(R_{ss'}(x) + R_{s's}(x)\big)
    \bigg]
    +\gamma_g^2 \sum_{x,s} \bigg[
      \frac{4 c_0}{3 u_s^2 u_t^2} P_{st}
      - \frac{1}{12 u_s^4 u_t^2} R_{st}(x)
    \bigg]
  \bigg\}.
\end{equation}

\subsection{Fermion action} \label{sec:fermion_action}

The fermionic action is as usual defined in terms of the Dirac operator
%
\begin{equation} \label{eq:aniso_dirac_op}
  D = \hat{m}_0 + \frac{1}{\tilde{u}_t} D_{W,t} + \frac{1}{\gamma_f \tilde{u}_t} D_{W,s}
  - \frac{1}{2} \bigg[
    \frac{c_T}{\tilde{u}_t^2 \tilde{u}_s^2} \sum_s i \sigma_{ts} \hat{F}_{ts}
    + \frac{c_R}{\xi_0 \tilde{u}_t \tilde{u}_s^3} \sum_{s<s'} i \sigma_{ss'} \hat{F}_{ss'}
  \bigg].
\end{equation}
%
Here $D_{W,t}$ and $D_{W,s}$ are the temporal and spatial hopping terms.
$\tilde{u}_s$ and $\tilde{u}_t$ are the fermionic tadpole improvement factors
for the spatial- and temporal gauge links respectively. $\gamma_f = \xi_0 / \nu$
is the bare fermionic anisotropy; in the input file we specify this through
$\nu$, which is the ratio of the gluonic and fermionic anisotropies. Finally
$c_T$ and $c_R$ are the temporal and spatial clover coefficients.

The anisotropy also changes the relationship between $\kappa$ and $m_0$, which
is now \cite{Chen:2000ej}
%
\begin{equation}
  \kappa = \frac{1}{2(\hat{m}_0 + 1 + 3\frac{\nu}{\xi_0})}
  \hspace{4mm}\Leftrightarrow\hspace{4mm}
  \hat{m}_0 = \frac{1}{2\kappa} - 1 - 3\frac{\nu}{\xi_0}.
\end{equation}

\subsection{Programming implementation}

\lstset{
  language=C,
  basicstyle=\ttfamily\fontsize{8pt}{8pt}\selectfont,
  identifierstyle=\color{SolBlue}}

For organisational purposes we list all the parts of the code touched by the
anisotropy implementation.

\begin{center}
  \setlength{\tabcolsep}{4mm}
  \begin{tabular}{>{\lstfont}l >{\lstfont}l >{\lstfont}l l} \toprule
    {\bfseries module} & {\bfseries file} & {\bfseries function} & {\bfseries Comment} \\\midrule
    dirac    & Dw.c         & * & aniso hopping coeffs, tadpole improvement \\
             & Dw\_dble.c   & * & \dittotikz \\
             & Dw\_bnd.c    & * & \dittotikz \\\midrule
    flags    & dfl\_parms.c & \lstinline!set_dfl_gen_parms! & $m_0 \leftrightarrow \kappa$ conversion \\
             & lat\_parms.c & \lstinline!set_lat_parms! & \dittotikz \\\midrule
    forces   & force0.c     & * & tadpole factors, aniso gauge coeffs, $R_{ts}$ toggle \\
             & force3.c     & \lstinline!sdet! & $m_0 \leftrightarrow \kappa$ conversion\\
             & force4.c     & \dittotikz & \dittotikz\\
             & genfrc.c     & \lstinline!sw_frc! & aniso clover coeffs, tadpole improvement \\
             &              & \lstinline!hop_frc! & aniso hopping coeffs, tadpole improvement \\\midrule
    little   & Aw\_ops.c    & \lstinline!set_Aw! & aniso hopping coeffs, tadpole improvement \\\midrule
    sw\_term & sw\_term.c   & * & aniso clover coeffs, tadpole improvement\\\bottomrule
  \end{tabular}
\end{center}

\printbibliography

\end{document}
