\documentclass[]{article}
\input{definitions/packages.tex}
\input{definitions/style.tex}
\input{definitions/macros.tex}

\addbibresource{references.bib}


% ------------- %
% Main document %
% ------------- %

\begin{document}

\noindent
\begin{tikzpicture}
  \node[scale=2] (title) {Propagators with \texttt{openqcd-propagator}}; 
  \draw[thick] (title.south west) -- +(\textwidth,0);
  \node[anchor=north west,font=\strut{}]
    at ([yshift=-1mm] title.south west) {Jonas Rylund Glesaaen};
  \node[anchor=north east,font=\strut{}]
    at ([shift={(\textwidth,-1mm)}] title.south west) {September 2018};
\end{tikzpicture}

\vspace{.5cm}

\section{Parameters for the propagator computation}

\lstset{%
  language={},
  basicstyle=\ttfamily\fontsize{8pt}{8pt}\selectfont,
  identifierstyle={}}

Although the infiles are for the most part identical to those in the vanilla
openQCD simulations, there are some differences. See the input file examples in
\texttt{main/examples} to see how they are specified. Here we will focus on
setting the sources for the propagator computations.
%
\begin{lstlisting}
    [Sources]
    num_sources  3
    random_dist  MAX_DISTANCE

    [Source 0]
    source  POINT_SOURCE
    point   0 0 0 0

    [Source 1]
    source       GAUSSIAN_SMEARED_SOURCE
    point        RANDOM
    smear_gauge  1
    num          60
    kappa        4.2

    [Source 2]
    source  POINT_SOURCE
    point   RANDOM
\end{lstlisting}
%
The \lstinline!num_sources! parameter tells the program which \lstinline!Source!
blocks to look for in the parameter file, and the \lstinline!random_dist!
parameter how sources with a random position should be distributed. The sources
are counted from 0 and have the following options:
%
\begin{description}[align=left]
  \item [source] \hfill\\
    The type of source; at the moment only point sources and Gaussian smeared
    sources are implemented. See the section \ref{sec:gaussian_smearing} for
    definition of the Gaussian smearing.
  \item [pos (int{[4]})] \hfill\\
    The placement of the source in global lattice coordinates. As with all
    openQCD applications these are $[n_t, n_x, n_y, n_z]$. Alternatively they
    can be given the value \lstinline!RANDOM!, in which case they are placed
    depending on the value of \lstinline!random_dist!.
  \item [smear\_gauge (boolean)] \hfill\\
    Whether or not to stout smear the gauge field before applying whatever operator is
    needed for the source type.
  \item [num (int)] \hfill ({\itshape req. for smeared source})\\
    Number of operations to apply for the source smearing.
  \item [kappa (double)] \hfill ({\itshape req. for smeared source})\\
    The $\kappa$ parameter for the Gaussian smearing operator. See the section
    \ref{sec:gaussian_smearing} for a definition.
\end{description}
%
For each of the sources specified in the infile, a separate propagator will be
written to the program output directory.

The \lstinline!random_dist! parameter can have one of two values,
\lstinline!RANDOM! or \lstinline!MAX_DISTANCE!. In the case of
\lstinline!RANDOM! the sources marked for random placement are all placed at
random, uncorrelated with each other. If \lstinline!MAX_DISTANCE! is chosen the
first random source is placed at random, then the remaining sources are placed
at corners of a hypercube with half the size of the global lattice. The random
sources are placed for every configuration, but not redistributed for different
mass parameters.

\section{The output and storage format}

\lstset{
  language=C,
  basicstyle=\ttfamily\fontsize{8pt}{8pt}\selectfont,
  identifierstyle=\color{SolBlue}}

To compute the propagator which is defined as $D^{-1}$ where $D$ is the
Dirac-operator we solve the equation
%
\begin{equation} \label{eq:dirac_eq}
  D(x,y)_{\alpha\beta}^{ab} \psi(y)_{\beta}^b = \phi(x)_{\alpha}^{a},
\end{equation}
%
for a source spinor $\phi$. If the source is a point source, namely if
%
\begin{equation}
  \phi(x)_{\alpha}^a = \delta(x - x_0) \delta_{\alpha,\alpha_0} \delta_{a, a_0},
\end{equation}
%
then the solution to the Dirac equation, $\psi$, gives a single column of the
propagator
%
\begin{equation}
  \psi(y)_{\beta}^b = \big(D^{-1}\big) (y, x_0)_{\beta, \alpha_0}^{b, a_0},
\end{equation}
%
i.e. the propagator from position $x$ with colour index $a_0$ and spinor index
$\alpha_0$ to position $y$ and colour index $b$ and spin index $\beta$. Since
one normally wants the full colour-spinor matrix of a propagator, the propagator
is defined as the solution of \eqref{eq:dirac_eq} for all 12 valid combinations
of $a_0$ and $\alpha_0$.

Internally the propagator is represented as an array of 12 spinor fields.
Although this can be a bit confusing, it is the structure that makes the most
sense with respect to how the propagator is computed as outlined in the previous
section. The propagator field is a global variable
%
\begin{lstlisting}[morekeywords={spinor_dble}]
    spinor_dble propagator_field[VOLUME][12];
\end{lstlisting}
%
which makes its index structure a bit confusing. Accessing the variable as
\lstinline!propagator_field[ix][3*i + j]! will give a
\lstinline[morekeywords={spinor_dble}]!spinor_dble! which is a struct containing
12 complex numbers, a spinor. In terms of the propagator this is
%
\begin{equation}
  \big(D^{-1}\big)(ix, x_0)_{\alpha, i}^{a, j}
\end{equation}
%
where $\alpha$ and $a$ are the remaining indices in the spinor struct.

\section{Gaussian smearing} \label{sec:gaussian_smearing}

One can define a smeared source by applying a smearing operator to a point
source. A smeared source is a field $\phi$ so that
%
\begin{equation}
  \phi = S \delta_{x_0}
\end{equation}
%
where $S$ is the smearing operator. Similar to how it is defined in
\cite{Gusken:1989ad} would ideally define the smearing operator as
%
\begin{equation}
  S = \big(\nabla_{\mathrm{3D}} + m^2\big)^{-1},
\end{equation}
%
i.e. as the propagator of the 3 dimensional Klein-Gordon operator. However, as
smearing is only an improvement operator to find better overlap with the ground
state of hadronic operators, we do not need an exact solution. Instead we can
approximate the solution in a way analogous to the hopping parameter expansion
and define
%
\begin{equation}
  S \sim \big(1 + \kappa H_{\mathrm{KG}} \big)^n.
\end{equation}

On the lattice the 3 dimensional Klein-Gordon operator is defined as
%
\begin{equation}
  D_{\mathrm{KG}}(x,y) = (6 + m^2)\delta_{x,y} - \sum_{i = 1}^3
  \big(\delta_{x,y+\hat{i}} + \delta_{x,y-\hat{i}}\big).
\end{equation}
%
To be consistent with the chroma software suite, making comparisons easier, the
final definition of the smearing operator is
%
\begin{equation}
  S_{\mathrm{GS}} = \Big(D_{\mathrm{KG}}(m^2 = m_{\mathrm{eff}}^2)
  \scalebox{0.85}{$\displaystyle\frac{1}{m_{\mathrm{eff}}^2}$} \Big)^n,
\end{equation}
%
where the effective mass is defined as
%
\begin{equation}
  m_{\mathrm{eff}}^2 = -\frac{4 n}{\kappa^2}.
\end{equation}

\printbibliography

\end{document}
