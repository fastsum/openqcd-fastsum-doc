\documentclass[]{article}
\input{definitions/packages.tex}
\input{definitions/style.tex}
\input{definitions/macros.tex}

\addbibresource{references.bib}


% ------------- %
% Main document %
% ------------- %

\begin{document}

\noindent
\begin{tikzpicture}
  \node[scale=2] (title) {Mu squared corrections with \texttt{openqcd-mu-squared}}; 
  \draw[thick] (title.south west) -- +(\textwidth,0);
  \node[anchor=north west,font=\strut{}]
    at ([yshift=-1mm] title.south west) {Jonas Rylund Glesaaen};
  \node[anchor=north east,font=\strut{}]
    at ([shift={(\textwidth,-1mm)}] title.south west) {September 2018};
\end{tikzpicture}

\vspace{.5cm}

\section{Parameters for the $\mu^2$ calculation}

\lstset{%
  language={},
  basicstyle=\ttfamily\fontsize{8pt}{8pt}\selectfont,
  identifierstyle={}}

Although the infiles are for the most part identical to those in the vanilla
openQCD simulations as well as those of openqcd-propagator. See the input file examples in
\texttt{main/examples} to see how they are specified. There are basically two
settings that determine which terms will be computed:
%
\begin{lstlisting}
    [Sources]
    num_sources  1
    random_dist  MAX_DISTANCE

    [Source 0]
    source  POINT_SOURCE
    point   0 0 0 0

    [Noise vectors]
    num 100
\end{lstlisting}
%
The \lstinline!Sources! parameter and sections are identical to those in
openqcd-propagator. The only new section is \lstinline!Noise vectors! which as
expected determines the number of noise vectors to use for the disconnected
terms.

For each of the sources specified in the infile, a separate propagator will be
written to the program output directory, these types of outputs are named
\lstinline!one-all!, whereas the noise vector terms are \lstinline!all-all!.

\section{Terms of the $\mu^2$ corrections}

This document is not intended to be a derivation of the $\mu^2$ corrections but
simply a list of definitions of the contributions and where they are stored in
the output. For a derivation of the terms see Davide De Boni's notes
\cite{musquared}.

We define the correlation function of a mesonic operator to be
%
\begin{equation}
  G = \big\langle \tr \big[ D^{-1}_{n,0} \Gamma D^{-1}_{0,n} \Gamma^{\dagger} \big] \big\rangle_B
\end{equation}
%
for a source at the origin ($0$). $\langle \cdot \rangle_B$ is the gauge
field average, $\tr$ (with a lower case t) is a colour-spin trace, and $\Gamma$
is any Euclidean gamma matrix. One can introduce a chemical potential dependence
by adding one to $D$, and carrying out a Taylor expansion around $\mu = 0$, the
first non-vanishing contribution appears at the second order. We are therefore
interested in the quantity $\ddot{G}|_{\mu = 0}$. Following the computation in
\cite{musquared} we get:
%
\begin{align} \label{eq:mu-squared}
  \ddot{G}\big|_{\mu = 0} = %
    &- 2 \re \big\langle \tr \big[ %
        \gamma_5 \Gamma^{\dagger} \big(D^{-1} \dot{D} D^{-1}\big)_{n,0}
        \Gamma \gamma_5 \big(D^{-1} \dot{D} D^{-1}\big)^{\dagger}_{n,0}
      \big] \big\rangle \nonumber \\
    &+ 4 \re \big\langle \tr \big[ %
        \gamma_5 \Gamma^{\dagger} \big(D^{-1} \dot{D} D^{-1} \dot{D} D^{-1}\big)_{n,0}
        \Gamma \gamma_5 \big(D^{-1}\big)^{\dagger}_{n,0}
      \big] \big\rangle \nonumber \\
    &- 2 \re \big\langle \tr \big[ %
        \gamma_5 \Gamma^{\dagger} \big(D^{-1} \ddot{D} D^{-1}\big)_{n,0}
        \Gamma \gamma_5 \big(D^{-1}\big)^{\dagger}_{n,0}
      \big] \big\rangle \nonumber \\
    &- 8\big\langle \im \tr \big[ %
        \gamma_5 \Gamma^{\dagger} \big(D^{-1} \dot{D} D^{-1}\big)_{n,0}
      \Gamma \gamma_5 \big(D^{-1}\big)^{\dagger}_{n,0} \big]
        \im \Tr \big[ \dot{D} D^{-1} \big]
      \big\rangle \nonumber \\
    &+ 2 \big\langle \tr \big[ %
        \gamma_5 \Gamma^{\dagger} \big(D^{-1}\big)_{n,0}
        \Gamma \gamma_5 \big(D^{-1}\big)^{\dagger}_{n,0}
      \big] \nonumber \\
    &\hspace{.5cm}\times \big( %
      2 \Tr\big[\dot{D} D^{-1}\big]^2 + \Tr\big[\ddot{D} D^{-1}\big] -
      \Tr\big[\big(\dot{D} D^{-1}\big)^2\big]
      \big)\big\rangle \nonumber \\
    &+ 2 \big\langle \tr \big[ %
        \gamma_5 \Gamma^{\dagger} \big(D^{-1}\big)_{n,0}
        \Gamma \gamma_5 \big(D^{-1}\big)^{\dagger}_{n,0}
      \big] \big\rangle \nonumber \\
    &\hspace{.5cm}\times  \big\langle \big( %
      2 \Tr\big[\dot{D} D^{-1}\big]^2 + \Tr\big[\ddot{D} D^{-1}\big] -
      \Tr\big[\big(\dot{D} D^{-1}\big)^2\big]
      \big)\big\rangle.
\end{align}
%
In this expression $\Tr$ (with an upper case T) denotes full
space-time-colour-spin traces, and are therefore disconnected diagrams, e.g.
"\lstinline!all-all!" contributions. We divide the expression into two parts,
the correlator like parts and the bubble-like parts.

\subsection{One-all contributions, correlator like}

The output from the executable stores all results in files with names
%
\begin{lstlisting}
    {run_name}n{config id}.one-all.s{source id}.m{mass id}
\end{lstlisting}
%
These are files with numbers in a human-readable format with 12 columns. The 12
columns are
%
\begin{lstlisting}
    {nt index}   | {gamma index} |
    term[1].real | term[1].imag  | 
    term[2].real | term[2].imag  | 
    term[3].real | term[3].imag  | 
    term[4].real | term[4].imag  | 
    term[5].real | term[5].imag
\end{lstlisting}
%
The 5 terms mentioned come from the correlator like terms in
\eqref{eq:mu-squared}
%
\begin{align}
  \mathrm{term}[1] = \;
    &\big\langle \tr \big[ %
        \gamma_5 \Gamma^{\dagger} \big(D^{-1} \dot{D} D^{-1} \dot{D} D^{-1}\big)_{n,0}
        \Gamma \gamma_5 \big(D^{-1}\big)^{\dagger}_{n,0}
      \big] \big\rangle, \\
  \mathrm{term}[2] = \;
    &\big\langle \tr \big[ %
        \gamma_5 \Gamma^{\dagger} \big(D^{-1} \ddot{D} D^{-1}\big)_{n,0}
        \Gamma \gamma_5 \big(D^{-1}\big)^{\dagger}_{n,0}
      \big] \big\rangle, \\
  \mathrm{term}[3] = \;
    &\big\langle \tr \big[ %
        \gamma_5 \Gamma^{\dagger} \big(D^{-1} \dot{D} D^{-1}\big)_{n,0}
        \Gamma \gamma_5 \big(D^{-1} \dot{D} D^{-1}\big)^{\dagger}_{n,0}
      \big] \big\rangle, \\
  \mathrm{term}[4] = \;
    &\big\langle \tr \big[ %
        \gamma_5 \Gamma^{\dagger} \big(D^{-1} \dot{D} D^{-1}\big)_{n,0}
        \Gamma \gamma_5 \big(D^{-1}\big)^{\dagger}_{n,0}
      \big] \big\rangle, \\
  \mathrm{term}[5] = \;
    &\big\langle \tr \big[ %
        \gamma_5 \Gamma^{\dagger} \big(D^{-1}\big)_{n,0}
        \Gamma \gamma_5 \big(D^{-1}\big)^{\dagger}_{n,0}
      \big] \big\rangle.
\end{align}
%
There will be $16 \times N_t$ rows for each file. To minimise the number of
inversions they are computed in order: $3 \to 4 \to 5 \to 1 \to 2$, we only need
$3 \times 12$ inversion in that case. It is apparent that all of the terms have
the form
%
\begin{equation}
  \big\langle \tr \big[ \gamma_5 \Gamma^{\dagger} A \Gamma \gamma_5 B^{\dagger} \big] \big\rangle,
\end{equation}
%
this general expression is therefore implemented in the function
\lstinline!full_trace_contribution()!.

\subsection{All-all contributions, bubble like}

For the all-all contributions we store them in files with names
%
\begin{lstlisting}
    {run_name}n{config id}.all-all.m{mass id}
\end{lstlisting}
%
These are files with numbers in a human-readable format with 6 columns. The 6
columns are
%
\begin{lstlisting}
    term[1].real | term[1].imag | term[2].real | term[2].imag | term[3].real | term[3].imag
\end{lstlisting}
%
Again, the 3 terms in this case also comes from the expression we are trying to
compute, namely \eqref{eq:mu-squared}
%
\begin{align}
  \mathrm{term}[1] = \;
    &\big\langle \Tr \big[ \dot{D} D^{-1}  \big] \big\rangle, \\
  \mathrm{term}[2] = \;
    &\big\langle \Tr \big[ \ddot{D} D^{-1}  \big] \big\rangle, \\
  \mathrm{term}[3] = \;
    &\big\langle \Tr \big[ \dot{D} D^{-1} \dot{D} D^{-1}  \big] \big\rangle.
\end{align}
%
There are the same number of rows as there are noise vectors in the computation,
one row per noise vector estimate.

\section{$\gamma$-matrix definitions}

We use the $\gamma$-matrices as defined by Lüscher \cite{dirac-operator}. Thus
we have
%
\begin{equation}
  \gamma_{\mu} = \begin{pmatrix} 0 & e_{\mu} \\ (e_{\mu})^{\dagger} & 0 \end{pmatrix}
  \hspace{.5cm} \text{where} \hspace{.5cm} e_0 = -1,\: e_i = - i \sigma_i.
\end{equation}
%
Furthermore, $\gamma_5 = \gamma_0 \gamma_1 \gamma_2 \gamma_3$, and
$\sigma_{\mu\nu} = \frac{i}{2} \big[ \gamma_{\mu}, \gamma_{\nu} \big]$. With
these definitions we define our 16 $\Gamma$-matrices to be
%
\begin{alignat*}{99}
  \Gamma_0 &= \mathbf{1}, & \hspace{1cm}
    \Gamma_4 &= \gamma_3, & \hspace{1cm}
    \Gamma_8 &= \gamma_5 \gamma_2, & \hspace{1cm}
    \Gamma_{12} &=  \sigma_{0, 3}, \\
  \Gamma_1 &= \gamma_0, &  \hspace{1cm}
    \Gamma_5 &= \gamma_5, & \hspace{1cm}
    \Gamma_9 &= \gamma_5 \gamma_3, & \hspace{1cm}
    \Gamma_{13} &= \sigma_{1,2}, \\
  \Gamma_2 &= \gamma_1, & \hspace{1cm}
    \Gamma_6 &= \gamma_5 \gamma_0, & \hspace{1cm}
    \Gamma_{10} &= \sigma_{0, 1}, & \hspace{1cm}
    \Gamma_{14} &= \sigma_{1,3}, \\
  \Gamma_3 &= \gamma_2, & \hspace{1cm}
    \Gamma_7 &= \gamma_5 \gamma_1, & \hspace{1cm}
    \Gamma_{11} &= \sigma_{0, 2}, & \hspace{1cm}
    \Gamma_{15} &= \sigma_{2,3}.
\end{alignat*}

\printbibliography

\end{document}
